// soal 1
var nilai = 90;

if (nilai >= 85 && nilai <= 100) {
    console.log("A");
} else if (nilai >= 75 && nilai < 85) {
    console.log("B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("C");
} else if (nilai >= 55 && nilai < 65) {
    console.log("D");
} else if (nilai >= 0 && nilai < 55) {
    console.log("E");
} 

// soal 2
var tanggal = 7;
var bulan = 8;
var tahun = 1999;

if (tanggal >= 1 && tanggal <= 31) {
    switch (bulan) {
        case 1: {
            console.log(tanggal + " Januari " + tahun);
            break;
        }
        case 2: {
            console.log(tanggal + " Februari " + tahun);
            break;
        }
        case 3: {
            console.log(tanggal + " Maret " + tahun);
            break;
        }
        case 4: {
            console.log(tanggal + " April " + tahun);
            break;
        }
        case 5: {
            console.log(tanggal + " Mei " + tahun);
            break;
        }
        case 6: {
            console.log(tanggal + " Juni " + tahun);
            break;
        }
        case 7: {
            console.log(tanggal + " Juli " + tahun);
            break;
        }
        case 8: {
            console.log(tanggal + " Agustus " + tahun);
            break;
        }
        case 9: {
            console.log(tanggal + " September " + tahun);
            break;
        }
        case 10: {
            console.log(tanggal + " Oktober " + tahun);
            break;
        }
        case 11: {
            console.log(tanggal + " November " + tahun);
            break;
        }
        case 12: {
            console.log(tanggal + " Desember " + tahun);
            break;
        }
    }
}

// soal 3
var n = 10;
for (let i = 1; i <= n; i++) {
    var temp = "";
    for (let j = 1; j <= i; j++) {
        temp += '#';
    }
    console.log(temp);
}

// soal 4
var m = 11;
for (var i = 1; i <= m; i++) {
    if (i % 3 == 1) {
        console.log(i + " - I love programming");
    } else if (i % 3 == 2) {
        console.log(i + " - I love Javascript");
    } else {
        console.log(i + " - I love VueJS\n===");
    }
}