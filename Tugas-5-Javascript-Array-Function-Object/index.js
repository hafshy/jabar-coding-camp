// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

for (var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan.sort()[i]);
}

// soal 2
function introduce(data) {
    return("Nama saya " + data["name"] + ", umur saya " + data["age"] + " tahun, alamat saya di " + data["address"] + ", dan saya punya hobby yaitu " + data["hobby"]);
}
var data = {
    name : "John",
    age : 30 ,
    address : "Jalan Pelesiran" ,
    hobby : "Gaming"
};
 
var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3
function hitung_huruf_vokal(kalimat) {
    var count = 0;
    for (var i = 0; i < kalimat.length; i++) {
        if (kalimat[i].toLowerCase() == "a" || kalimat[i].toLowerCase() == "i" || kalimat[i].toLowerCase() == "u" || kalimat[i].toLowerCase() == "e" || kalimat[i].toLowerCase() == "o") {
            count++;
        }
    }
    return(count);
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// soal 4
function hitung(number) {
    return((number - 1) * 2);
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8


function jumlah_kata(kalimat) {
    var count = 1;
    
    if (kalimat.length == 0) {
        return 0;
    } else {
        for (var i = 0; i < kalimat.length; i++) {
            if (kalimat[i] == " ") {
                count++;
            }
        }
    }
}

function isKabisat(tahun) {
    return ((tahun % 4 == 0) && (tahun % 100 != 0)) || (tahun % 400 == 0);
}

function isAkhirTahun(tanggal, bulan) {
    return (tanggal == 31 && bulan == 12);
}

function isAkhirBulan(tanggal, bulan, tahun) {
    return (isKabisat(tahun) && bulan == 2 && tanggal == 29)
    || (!isKabisat(tahun) && bulan == 2 && tanggal == 28)
    || ((bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10) && (tanggal == 31))
    || ((bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) && (tanggal == 30));
}

function intToMonth(bulan) {
    switch (bulan) {
        case 1: {
            return "Januari";
        }
        case 2: {
            return "Februari";
        }
        case 3: {
            return "Maret";
        }
        case 4: {
            return "April";
        }
        case 5: {
            return "Mei";
        }
        case 6: {
            return "Juni";
        }
        case 7: {
            return "Juli";
        }
        case 8: {
            return "Agustus";
        }
        case 9: {
            return "September";
        }
        case 10: {
            return "Oktober";
        }
        case 11: {
            return "November";
        }
        case 12: {
            return "Desember";
        }
    }
}

function next_date(tanggal, bulan, tahun) {
    if (isAkhirTahun(tanggal, bulan)) {
        console.log("1 Januari " + (tahun + 1).toString());
    } else if (isAkhirBulan(tanggal, bulan, tahun)) {
        console.log("1 " + intToMonth(bulan + 1).toString() + " " + tahun.toString());
    } else {
        console.log((tanggal + 1).toString() + " " + intToMonth(bulan).toString() + " " + tahun.toString());
    }
}

next_date(29, 2, 2000);

function jumlah_kata(kalimat) {
    var count = 1;

    if (kalimat.length == 0) {
        return 0;
    } else {
        for (var i = 0; i < kalimat.length; i++) {
            if (kalimat[i] == " ") {
                count++;
            }
        }
        return count;
    }
}

console.log(jumlah_kata(""));