
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

function readBook1(cb) {
    readBooks(cb, books[1], readBook2);
}

function readBook2(cb) {
    readBooks(cb, books[2], readBook3);
}

function readBook3(cb) {
    readBooks(cb, books[3], () => {});
}

readBooks(10000, books[0], readBook1);