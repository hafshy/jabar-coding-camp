var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

function readBook1Promise(cb) {
    readBooksPromise(cb, books[1]).then(readBook2Promise);
}

function readBook2Promise(cb) {
    readBooksPromise(cb, books[2]).then(readBook3Promise);
}

function readBook3Promise(cb) {
    readBooksPromise(cb, books[3]);
}
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
readBooksPromise(10000, books[0]).then(readBook1Promise);